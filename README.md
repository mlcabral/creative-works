This repository contains some of my current and past creative works. Specifically, some old mods I made for a couple Need For Speed games and some LEGO models that I've designed as a pastime.

(this is just a temporary host for these works until I setup my personal page)

I also have a small collection of obfuscated C programs that I made for fun and to explore more ways to (ab)use GCC in a [separate repository](https://gitlab.com/mlcabral/obfuscated-c).
